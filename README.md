# CAZANA CODING TEST #

This is a coding test with terms in
the [PDF File](https://bitbucket.org/Mulder1/coding_test/raw/120752cf035e824b9e6e09f7fbcc8777ea091f97/Cazana_back-end_coding_test.pdf)

You can get this coding test from Bitbucket:

~~~
git clone https://bitbucket.org/Mulder1/coding_test.git
~~~


### Program installation ###

This program only needs to python 3.x installed

You can download from [Python.org](https://www.python.org/downloads/)

It requires to install the request library:

~~~
pip3 install -r requirements.txt
~~~


### Executing the program ###

For execute main code:

~~~
./car_mileage_calc.py [plate number]
~~~

This program has a list of plates numbers already hardcoded but you can
use one plate number as argument always it has an A on it, this restriction
is a limit for the free usage of API for getting car data.


### Executing the tests ###

For execute unit tests:

~~~
./tests.py [plate number]
~~~

The tests could accept a random plate number too, that always has an A on it.



