#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys
from car_mileage_calc import vehicleData


def test_load_csv():
    vd.advertiseds = vd.read_csv("./advertisements.txt")
    assert len(vd.advertiseds) == 15


def test_get_seconds():
    result = vd.get_seconds("2021-01-01 00:00:00", "%Y-%m-%d %H:%S:%M")
    assert result == 1609455600

def test_get_vrm_data(data):
    assert(len(data)) == 2


def test_fill_tables(data, vid):
    vd.advertiseds = vd.read_csv("./advertisements.txt") 
    vd.fill_tables(data, vid)
    assert len(vd.vehicles) == 1


# -- this is done by defining previous data and asserting --
def test_calc_mileage():
    vd.vehicles = [{
        'id': 6, 
        'vrm': 'M111YAP', 
        'make': 'VOLKSWAGEN', 
        'model': 'PASSAT GT CC TSI 210 S-A', 
        'registration': '2011-04-15T00:00:00'
    }]
    vd.mot_tests = [
        {'vrm': 'M111YAP', 'date': '25/07/2020', 'mileage': 133410, 
        'result': 'Pass'},
        {'vrm': 'M111YAP', 'date': '27/07/2019', 'mileage': 130864, 
        'result': 'Pass'},
        {'vrm': 'M111YAP', 'date': '28/07/2018', 'mileage': 128027, 
        'result': 'Pass'},
        {'vrm': 'M111YAP', 'date': '21/04/2018', 'mileage': 127514, 
        'result': 'Pass'},
        {'vrm': 'M111YAP', 'date': '22/04/2017', 'mileage': 121772, 
        'result': 'Pass'},
        {'vrm': 'M111YAP', 'date': '23/04/2016', 'mileage': 116031, 
        'result': 'Pass'},
        {'vrm': 'M111YAP', 'date': '24/03/2015', 'mileage': 101658, 
        'result': 'Pass'},
        {'vrm': 'M111YAP', 'date': '24/03/2014', 'mileage': 74401, 
        'result': 'Pass'}
    ]
    result = vd.calc_mileage(vd.vehicles[0])
    assert result['avg_mileage'] == 13519
    assert result['estimated'] == 8381


###############################################################################
## MAIN

vd = vehicleData()


# -- set a default plate and get arguments if any --
plate = "MA11WOX"
if len(sys.argv) > 1:
    plate = sys.argv[1]


# -- tests block --
test_load_csv()
data = vd.get_vrm_data(plate)
test_get_vrm_data(data)
test_fill_tables(data, 1)
test_calc_mileage()
