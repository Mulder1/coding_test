#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys
import time
import csv
import requests
from operator import itemgetter


class vehicleData:
    def __init__(self):
        # -- definitions for getting data --
        self.vrmlist = ['BU67LXA', 'AD17AYM', 'MA11WOX', 'CN18HAA', 'HV52XHA',
                'M111YAP', 'YT19FTA', 'KM68HAE', 'EU67NAA', 'AU17YHL',
                'JA16CSA', 'SV66WHA', 'NG16RKA', 'AK67JWW', 'YT61DAU',
                'BW66WTA', 'EA66AZT', 'X29CAB', 'DY19CAO', 'G9ALP',
                'C38OLA', 'FD19POA', 'C8DAO', 'MA19ZSY', 'EA19VVT',
                'LJ06GHA', 'SU54MAT', 'YP18BWA', 'V12PAL', 'RA65NWO',
                'BT15ADU', 'AJ18WZT', 'AP19TZW', 'CA56RMU', 'DA68BLV', 
                'AU18XTH', 'HG65ABU', 'DY18ODA', 'EY17VHA', 'SF18XAK']

        self.api_key = "ea80526d-609c-41ab-9923-82aeb14918c2"
        self.data_packages = ["VehicleData", "MotHistoryAndTaxStatusData"]
        self.url = "https://uk1.ukvehicledata.co.uk/api/datapackage/"
        self.avg_mileage = 7900

        # -- vehicle tables definition --
        self.vehicles = []
        self.advertiseds = []
        self.mot_tests = []
        self.vrm_changes = []

        return


    # -- load csv file --
    def read_csv(self, csv_file: str) -> dict:
        csv_data = []

        with open(csv_file, "rt") as csvfile:
            data = csv.DictReader(csvfile, delimiter=',', quotechar='"')
            for row in data:
                csv_data.append(dict(row))

        return csv_data


    # -- get seconds from date --
    def get_seconds(self, orig_date: str, date_format: str) -> int:
        seconds = 0

        tmpsecs = time.strptime(orig_date, date_format)
        seconds = int(time.mktime(tmpsecs))

        return seconds


    # -- get vehicle data --
    def get_vrm_data(self, vrm: str) -> str:
        data = []
        payload = {
            'v': 2,
            'api_nullitems': 1,
            'key_vrm': vrm,
            'auth_apikey': self.api_key
        }

        # -- get URL --
        for package in self.data_packages:
            url = self.url+package
            result = requests.get(url, params=payload)

            # -- process data --
            if result.status_code == requests.codes.ok:
                data.append(result.json())
            else:
                data.append({'error': "ERROR: Status Code: {}" \
                            ", Reason: {}".format(r.status_code, r.reason)})
                return data

        return data


    # -- fill tables with JSON data --
    def fill_tables(self, data: list, vid: int) -> None:
        mot_tests = []
        veh_data = {}
        reg_data = {}

        if veh_data is not None:
            veh_data = data[0]['Response']['DataItems']
        if reg_data is not None:
            reg_data = data[1]['Response']['DataItems']

        self.vehicles.append({
            'id': vid,
            'vrm': veh_data['VehicleRegistration']['Vrm'],
            'make': veh_data['ClassificationDetails']['Dvla']['Make'],
            'model': veh_data['ClassificationDetails']['Dvla']['Model'],
            'registration': 
                veh_data['VehicleRegistration']['DateFirstRegisteredUk']
        })

        for mot_test in reg_data['MotHistory']['RecordList']:
            self.mot_tests.append({
                'vrm': veh_data['VehicleRegistration']['Vrm'],
                'date': mot_test['TestDate'],
                'mileage': mot_test['OdometerInMiles'],
                'result': mot_test['TestResult']
            })

        if veh_data['VehicleHistory']['PlateChangeList'] is not None:
            for change in veh_data['VehicleHistory']['PlateChangeList']:
                self.vrm_changes.append({
                    'vrm': veh_data['VehicleRegistration']['Vrm'],
                    'date': change['DateOfTransaction'],
                    'from_vrm': change['PreviousVRM'],
                    'to_vrm': change['CurrentVRM']
                })

        return


    # -- calculate mileage --
    def calc_mileage(self, vehicle: dict) -> dict:
        mileages = {
            'avg_mileage': self.avg_mileage,
            'estimated': 0
        }
        sum_mileage = 0
        old_mileage = 0
        today_secs = int(time.time())
        events = []

        # -- get events and sort --
        for event in self.mot_tests:
            if event['vrm'] == vehicle['vrm']:
                event['seconds'] = self.get_seconds(event['date'], "%d/%m/%Y")
                events.append(event)
        events = sorted(events, key=itemgetter("seconds"))

        # -- define years and default estimated mileage --
        today_secs = int(time.time())
        begin_secs = self.get_seconds(vehicle['registration'], 
                                    "%Y-%m-%dT%H:%M:%S")

        years = (today_secs - begin_secs) / 31536000
        mileages['estimated'] = years * self.avg_mileage

        if len(events) > 0:
            # -- define years --
            today_secs = int(time.time())
            years = (today_secs - begin_secs) / 31536000

            # -- calculate mileage with events --
            for event in events:
                sum_mileage += event['mileage'] - old_mileage
                old_mileage = event['mileage']

            mileages['avg_mileage'] = int(sum_mileage / years)

            # -- calculate estimated mileage since last event --
            last_mot_secs = self.get_seconds(events[-1]['date'], "%d/%m/%Y")
            mileages['estimated'] = int(((today_secs - last_mot_secs) \
                                    * sum_mileage) / (last_mot_secs - begin_secs))

        return mileages


    # -- main --
    def main(self):
        # -- get file stored data --
        self.advertiseds = self.read_csv("./advertisements.txt")

        # -- process vehicle data --
        for vid, vrm in enumerate(self.vrmlist, start=1):
            print("VRM: {}".format(vrm))
            result = self.get_vrm_data(vrm)
            if not 'error' in result:
                self.fill_tables(result, vid)
            time.sleep(2)

        # -- show data --
        print("Vehicles:")
        for vehicle in self.vehicles:
            print(vehicle)
        print("Advertisements:")
        for add in self.advertiseds:
            print(add)
        print("MOT Test:")
        for mot in self.mot_tests:
            print(mot)
        print("VRM Changes:")
        for changes in self.vrm_changes:
            print(changes)
        print("----------------")

        # -- calculate mileages for each car in plates --
        for vehicle in self.vehicles:
            result = self.calc_mileage(vehicle)
            print("Vehicle:")
            print(vehicle)
            print("1.- AVG.MILEAGE:")
            print(result['avg_mileage'])
            print("2.- ESTIMATED SINCE LAST MOT:")
            print(result['estimated'])

        return


##############################################################################
## MAIN

vd = vehicleData()

if __name__ == "__main__":
    if len(sys.argv) > 1:
        vd.vrmlist = sys.argv[1:]
    vd.main()

